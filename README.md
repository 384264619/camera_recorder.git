# camera_recorder

A package that uses a camera to record video and take pictures
一个使用摄像头录制视频与拍照的包

# performance problem !!

After the video recording with the front-facing camera is finished, the FFMEPG is used to perform the horizontal flipping of the video, which will take different processing time depending on the performance of the device and the video size. Therefore, it is recommended to control the video size within 720P and 1 minute.

因为使用前置摄像头录制视频结束后,使用ffmepg作了视频水平翻转,此过程会因设备性能及视频大小,在处理时间有所不同,所以建议拍照视频大小控制在720p,1分钟以内.


# Installing
Add this to your package's pubspec.yaml file:

```dart
dependencies:
  camera_recorder: ^latest
```  



# how to use
initialize EasyLoading(which this package use loading dialog) in your MaterialApp/CupertinoApp:
```dart
import 'package:flutter/material.dart';
import 'package:camera_recorder/camera_recorder.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter EasyLoading',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TextButton(
          onPressed: () async {
            final AssetEntity? result =
                await CameraView.showCameraView(context);
          },
          child: Text('show camera')),
      builder: EasyLoading.init(),
    );
  }
}


```