import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:camera_recorder/camera_recorder.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter EasyLoading',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
      builder: EasyLoading.init(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _SplashPageState();
}

class _SplashPageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).canvasColor,
      child: Center(
        child: TextButton(
            onPressed: () async {
              final AssetEntity? result = await CameraView.showCameraView(
                  context,
                  mode: CameraMode.record);
              log(result?.relativePath ?? '');
            },
            child: const Text('show camera')),
      ),
    );
  }
}
